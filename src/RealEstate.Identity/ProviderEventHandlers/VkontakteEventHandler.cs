﻿using Microsoft.AspNet.Authentication.OAuth;
using Newtonsoft.Json.Linq;
using RealEstate.Identity.Helpers;
using System.Threading.Tasks;
using Microsoft.AspNet.Authentication;
using System;

namespace RealEstate.Identity.ProviderEventHandlers
{
    public class VkontakteEventHandler : OAuthEvents
    {
        public override Task CreatingTicket(OAuthCreatingTicketContext context)
        {
            var uid = context.User.SelectToken("response[0].uid").Value<string>();
            var userName = $"{context.Options.AuthenticationScheme}{uid}";
            var displayName = $"{context.User.SelectToken("response[0].first_name").Value<string>()} {context.User.SelectToken("response[0].last_name").Value<string>()}";
            var photoUrl = context.User.SelectToken("response[0].photo_100").Value<string>();

            ClaimHelper.SetProviderClaims(context.Identity, uid, userName, displayName, photoUrl);

            return base.CreatingTicket(context);
        }
    }
}
