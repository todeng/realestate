﻿using Microsoft.AspNet.Authentication.OAuth;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RealEstate.Identity.Helpers;
using Microsoft.AspNet.WebUtilities;

namespace RealEstate.Identity.ProviderEventHandlers
{
    public class FacebookEventHandler : OAuthEvents
    {
        public override Task CreatingTicket(OAuthCreatingTicketContext context)
        {
            var uid = context.User.SelectToken("id").Value<string>();
            var userName = $"{context.Options.AuthenticationScheme}{uid}";
            var displayName = $"{context.User.SelectToken("first_name").Value<string>()} {context.User.SelectToken("last_name").Value<string>()}";
            var photoUrl = $"https://graph.facebook.com/{uid}/picture?type=normal";

            ClaimHelper.SetProviderClaims(context.Identity, uid, userName, displayName, photoUrl);

            return base.CreatingTicket(context);
        }

        public override Task RedirectToAuthorizationEndpoint(OAuthRedirectToAuthorizationContext context)
        {
            OAuthRedirectToAuthorizationContext newContext = new OAuthRedirectToAuthorizationContext(
                context.HttpContext, context.Options, context.Properties, QueryHelpers.AddQueryString(context.RedirectUri, "display", "popup"));
            return base.RedirectToAuthorizationEndpoint(newContext);
        }
    }
}
