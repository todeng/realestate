﻿using RealEstate.Identity.Defaults;
using System.Security.Claims;

namespace RealEstate.Identity.Helpers
{
    public static class ClaimHelper
    {
        public static void SetProviderClaims(this ClaimsIdentity identity,
            string uid, string userName, string displayName, string photoUrl)
        {
            identity.AddClaim(new Claim(AuthProvidersClaims.Uid, uid, ClaimValueTypes.String));
            identity.AddClaim(new Claim(AuthProvidersClaims.UserName, userName, ClaimValueTypes.String));
            identity.AddClaim(new Claim(AuthProvidersClaims.DisplayName, displayName, ClaimValueTypes.String));
            identity.AddClaim(new Claim(AuthProvidersClaims.PhotoUrl, photoUrl, ClaimValueTypes.String));
        }

        public static string GetName(this ClaimsIdentity identity)
        {
            Claim claim = identity.FindFirst(ClaimTypes.Name);
            return claim?.Value;
        }

        public static string GetNameIdentifer(this ClaimsIdentity identity)
        {
            Claim claim = identity.FindFirst(ClaimTypes.NameIdentifier);
            return claim?.Value;
        }

        public static string GetUid(this ClaimsIdentity identity)
        {
            Claim claim = identity.FindFirst(AuthProvidersClaims.Uid);
            return claim?.Value;
        }

        public static string GetUserName(this ClaimsIdentity identity)
        {
            Claim claim = identity.FindFirst(AuthProvidersClaims.UserName);
            return claim?.Value;
        }

        public static string GetDisplayName(this ClaimsIdentity identity)
        {
            Claim claim = identity.FindFirst(AuthProvidersClaims.DisplayName);
            return claim?.Value;
        }

        public static string GetPhotoUrl(this ClaimsIdentity identity)
        {
            Claim claim = identity.FindFirst(AuthProvidersClaims.PhotoUrl);
            return claim?.Value;
        }
    }
}
