﻿namespace RealEstate.Identity.Providers.Vkontakte
{
    public static class VkontakteAuthenticationDefaults
    {
        public const string AuthenticationScheme = "Vkontakte";

        public const string AuthorizationEndpoint = "https://oauth.vk.com/authorize";

        public const string TokenEndpoint = "https://oauth.vk.com/access_token";

        public const string UserInformationEndpoint = "https://api.vk.com/method/users.get";

        public const string ApiVersion = "5.35";
    }
}
