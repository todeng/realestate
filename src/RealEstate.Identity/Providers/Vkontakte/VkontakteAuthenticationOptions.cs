﻿using Microsoft.AspNet.Authentication.OAuth;
using Microsoft.AspNet.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstate.Identity.Providers.Vkontakte
{
    public class VkontakteAuthenticationOptions : OAuthOptions
    {
        public VkontakteAuthenticationOptions()
        {
            AuthenticationScheme = VkontakteAuthenticationDefaults.AuthenticationScheme;
            DisplayName = AuthenticationScheme;
            CallbackPath = new PathString("/signin-vkontakte");
            AuthorizationEndpoint = VkontakteAuthenticationDefaults.AuthorizationEndpoint;
            TokenEndpoint = VkontakteAuthenticationDefaults.TokenEndpoint;
            UserInformationEndpoint = VkontakteAuthenticationDefaults.UserInformationEndpoint;
            SaveTokensAsClaims = false;
            DisplayType = VkontakteDisplayType.Popup;
        }

        public VkontakteDisplayType DisplayType { get; set; }

        public ICollection<string> Fields { get; } = new HashSet<string>();
    }
}
