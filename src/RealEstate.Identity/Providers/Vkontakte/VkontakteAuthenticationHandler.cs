﻿using Microsoft.AspNet.Authentication;
using Microsoft.AspNet.Authentication.OAuth;
using Microsoft.AspNet.Http.Authentication;
using Microsoft.AspNet.Http.Extensions;
using Microsoft.AspNet.WebUtilities;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RealEstate.Identity.Providers.Vkontakte
{
    internal class VkontakteAuthenticationHandler : OAuthHandler<VkontakteAuthenticationOptions>
    {
        public VkontakteAuthenticationHandler(HttpClient httpClient) : base(httpClient)
        {
        }

        protected override string BuildChallengeUrl(AuthenticationProperties properties, string redirectUri)
        {
            var display = GetDisplayType(Options.DisplayType);
            var state = Options.StateDataFormat.Protect(properties);
            var scope = FormatScope();

            var queryBuilder = new QueryBuilder
            {
                {"client_id", Options.ClientId},
                {"redirect_uri", redirectUri},
                {"display", display},
                {"scope", scope},
                {"response_type", "code"},
                {"v", VkontakteAuthenticationDefaults.ApiVersion},
                {"state", state}
            };

            return Options.AuthorizationEndpoint + queryBuilder;
        }

        protected override async Task<OAuthTokenResponse> ExchangeCodeAsync(string code, string redirectUri)
        {
            var queryBuilder = new QueryBuilder
            {
                {"client_id", Options.ClientId},
                {"client_secret", Options.ClientSecret},
                {"redirect_uri", redirectUri},
                {"code", code}
            };

            var response = await Backchannel.GetAsync(Options.TokenEndpoint + queryBuilder, Context.RequestAborted);
            response.EnsureSuccessStatusCode();

            string json;
            using (var content = response.Content)
            {
                json = await content.ReadAsStringAsync();
            }

            var payload = JObject.Parse(json);
            return new OAuthTokenResponse(payload);
        }

        protected override async Task<AuthenticationTicket> CreateTicketAsync(ClaimsIdentity identity, AuthenticationProperties properties, OAuthTokenResponse tokens)
        {
            var endpoint = QueryHelpers.AddQueryString(Options.UserInformationEndpoint, "access_token", tokens.AccessToken);
            if (Options.Fields.Count > 0)
            {
                endpoint = QueryHelpers.AddQueryString(endpoint, "fields", string.Join(",", Options.Fields));
            }
            var response = await Backchannel.GetAsync(endpoint, Context.RequestAborted);
            response.EnsureSuccessStatusCode();

            var payload = JObject.Parse(await response.Content.ReadAsStringAsync());

            var ticket = new AuthenticationTicket(new ClaimsPrincipal(identity), properties, Options.AuthenticationScheme);
            var context = new OAuthCreatingTicketContext(Context, Options, Backchannel, tokens, payload);
            context.Principal = ticket.Principal;

            var userId = VkontakteHelper.GetId(payload);
            if (userId != null)
            {
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userId, ClaimValueTypes.String, Options.ClaimsIssuer));
                identity.AddClaim(new Claim("urn:facebook:uid", userId, ClaimValueTypes.String, Options.ClaimsIssuer));
            }

            var userFirstName = VkontakteHelper.GetFirstName(payload);
            if (userId != null)
            {
                identity.AddClaim(new Claim("urn:facebook:first_name", userFirstName, ClaimValueTypes.String, Options.ClaimsIssuer));
            }

            var userLastName = VkontakteHelper.GetLastName(payload);
            if (userId != null)
            {
                identity.AddClaim(new Claim("urn:facebook:last_name", userLastName, ClaimValueTypes.String, Options.ClaimsIssuer));
            }

            await Options.Events.CreatingTicket(context);

            return ticket;
        }

        protected override string FormatScope()
        {
            return string.Join(",", Options.Scope);
        }

        private static string GetDisplayType(VkontakteDisplayType displayType)
        {
            switch (displayType)
            {
                case VkontakteDisplayType.Page:
                    {
                        return "page";
                    }
                case VkontakteDisplayType.Popup:
                    {
                        return "popup";
                    }
                case VkontakteDisplayType.Mobile:
                    {
                        return "mobile";
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException(nameof(displayType), displayType, null);
                    }
            }
        }
    }
}
