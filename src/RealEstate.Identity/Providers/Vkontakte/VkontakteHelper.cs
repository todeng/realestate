﻿using Newtonsoft.Json.Linq;
using System;

namespace RealEstate.Identity.Providers.Vkontakte
{
    public static class VkontakteHelper
    {
        public static string GetId(JObject user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return user.SelectToken("response[0].uid").Value<string>();
        }

        public static string GetFirstName(JObject user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return user.SelectToken("response[0].first_name").Value<string>();
        }

        public static string GetLastName(JObject user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return user.SelectToken("response[0].last_name").Value<string>();
        }
    }
}
