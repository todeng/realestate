﻿using Microsoft.AspNet.Authentication;
using Microsoft.AspNet.Authentication.OAuth;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.DataProtection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.OptionsModel;
using Microsoft.Extensions.WebEncoders;

namespace RealEstate.Identity.Providers.Vkontakte
{
    public class VkontakteAuthenticationMiddleware : OAuthMiddleware<VkontakteAuthenticationOptions>
    {
        public VkontakteAuthenticationMiddleware(RequestDelegate next,
                                                 IDataProtectionProvider dataProtectionProvider,
                                                 ILoggerFactory loggerFactory,
                                                 IUrlEncoder encoder,
                                                 IOptions<SharedAuthenticationOptions> sharedOptions,
                                                 VkontakteAuthenticationOptions options)
            : base(next, dataProtectionProvider, loggerFactory, encoder, sharedOptions, options)
        {
        }

        protected override AuthenticationHandler<VkontakteAuthenticationOptions> CreateHandler()
        {
            return new VkontakteAuthenticationHandler(Backchannel);
        }
    }
}
