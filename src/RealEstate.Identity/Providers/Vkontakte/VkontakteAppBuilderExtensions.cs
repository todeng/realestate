﻿using Microsoft.AspNet.Builder;
using Microsoft.Extensions.OptionsModel;
using System;

namespace RealEstate.Identity.Providers.Vkontakte
{
    public static class VkontakteAppBuilderExtensions
    {
        public static IApplicationBuilder UseVkontakteAuthentication(this IApplicationBuilder app, Action<VkontakteAuthenticationOptions> configureOptions = null, string optionsName = "")
        {
            VkontakteAuthenticationOptions options = new VkontakteAuthenticationOptions();
            var optionsConfigurator = new ConfigureOptions<VkontakteAuthenticationOptions>(configureOptions ?? (_ => {}));
            optionsConfigurator.Configure(options);

            return app.UseMiddleware<VkontakteAuthenticationMiddleware>(options);
        }
    }
}
