﻿namespace RealEstate.Identity.Providers.Vkontakte
{
    public enum VkontakteDisplayType
    {
        Page,
        Popup,
        Mobile,
    }
}
