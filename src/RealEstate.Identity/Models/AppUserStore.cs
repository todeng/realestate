﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using RealEstate.Domain.Entities;
using RealEstate.Services.Interfaces;
using RealEstate.Identity.Defaults;

namespace RealEstate.Identity
{
    public class AppUserStore : IUserStore<User>,
                                IUserPasswordStore<User>,
                                IUserSecurityStampStore<User>,
                                IUserLoginStore<User>,
                                IUserClaimStore<User>
    {

        private readonly IUserService _userService;

        public AppUserStore(IUserService userService)
        {
            _userService = userService;
        }

        public Task AddClaimsAsync(User user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task AddLoginAsync(User user, UserLoginInfo login, CancellationToken cancellationToken)
        {
            return null;
            //throw new NotImplementedException();
        }

        public Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => {
                try
                {
                    _userService.Add(user);
                    return IdentityResult.Success;
                }
                catch(Exception)
                {
                    return IdentityResult.Failed();
                }
            }, cancellationToken);
        }

        public Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => {
                try
                {
                    _userService.Remove(user);
                    return IdentityResult.Success;
                }
                catch (Exception e)
                {
                    return IdentityResult.Failed();
                }
            }, cancellationToken);
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => _userService.Get(int.Parse(userId)), cancellationToken);
        }

        public Task<User> FindByLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => _userService.FindByProvider(loginProvider, providerKey), cancellationToken);
        }

        public Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => _userService.FindByUserName(normalizedUserName), cancellationToken);
        }

        public Task<IList<Claim>> GetClaimsAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew< IList<Claim>>(() =>
            {
                List<Claim> claims = new List<Claim>();
                claims.Add(new Claim(AuthProvidersClaims.DisplayName,  user.DisplayName));
                claims.Add(new Claim(AuthProvidersClaims.UserName, user.UserName));
                claims.Add(new Claim(AuthProvidersClaims.PhotoUrl, user.PictureUrl));

                return claims;
            });
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew<IList<UserLoginInfo>>(() => new List<UserLoginInfo>() {
                new UserLoginInfo(user.Provider, user.ProviderKey, user.UserName) }, cancellationToken);
        }

        public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => user.UserName.ToLower(), cancellationToken);
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => user.PasswordHash, cancellationToken);
        }

        public Task<string> GetSecurityStampAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => user.SecurityStamp, cancellationToken);
        }

        public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => user.Id.ToString(), cancellationToken);
        }

        public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => user.UserName, cancellationToken);
        }

        public Task<IList<User>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => !string.IsNullOrEmpty(user.PasswordHash), cancellationToken);
        }

        public Task RemoveClaimsAsync(User user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task RemoveLoginAsync(User user, string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task ReplaceClaimAsync(User user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => user.UserName = normalizedName, cancellationToken);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => user.PasswordHash = passwordHash, cancellationToken);

        }

        public Task SetSecurityStampAsync(User user, string stamp, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => user.SecurityStamp = stamp, cancellationToken);
        }

        public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
