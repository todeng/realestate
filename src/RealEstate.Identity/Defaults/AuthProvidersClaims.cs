﻿namespace RealEstate.Identity.Defaults
{
    public sealed class AuthProvidersClaims
    {
        public const string Uid = "realestate:uid";
        public const string UserName = "realestate:userName";
        public const string DisplayName = "realestate:displayName";
        public const string PhotoUrl = "realestate:photoUrl";
    }
}
