﻿using RealEstate.Domain.Entities;
using RealEstate.Domain.UoW;
using RealEstate.Services.Interfaces;

namespace RealEstate.Services.Implementations
{
    public class ApartmentService : ModelService<Apartment>, IApartmentService
    {
        private readonly IUserService _userService;

        public ApartmentService(IUnitOfWork unitOfWork, IUserService userService):base(unitOfWork)
        {
            _userService = userService;
        }

        public void Add(int userId, Apartment model)
        {
            
        }
    }
}
