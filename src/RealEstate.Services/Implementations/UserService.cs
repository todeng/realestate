﻿using System;
using RealEstate.Domain.Entities;
using RealEstate.Domain.UoW;
using RealEstate.Services.Interfaces;
using RealEstate.Domain.Repository;
using System.Linq;

namespace RealEstate.Services.Implementations
{
    public class UserService : ModelService<User>, IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<User> _userRepository;

        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _userRepository = unitOfWork.Users;
        }

        public User FindByUserName(string userName)
        {
            return _userRepository.Get().SingleOrDefault(x => string.Equals(x.UserName, userName));
        }

        public User FindByProvider(string provider, string providerKey)
        {
            return _userRepository.Get().SingleOrDefault(x => string.Equals(x.Provider, provider) && string.Equals(x.ProviderKey, providerKey));
        }
    }
}
