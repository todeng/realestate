﻿using System.Collections.Generic;
using RealEstate.Domain.Entities;
using RealEstate.Domain.UoW;
using RealEstate.Domain.Repository;
using RealEstate.Services.Interfaces;
using System.Threading.Tasks;

namespace RealEstate.Services.Implementations
{
    public class ModelService<T> : IModelService<T>  where T : IBaseEntity
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly IRepository<T> _modelRepository;

        public ModelService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _modelRepository = unitOfWork.GetRepository<T>();
        }

        public T Add(T model)
        {
            var entity = _modelRepository.Add(model);
            _unitOfWork.Save();

            return entity;
        }

        public IEnumerable<T> GetAll()
        {
            return _modelRepository.GetAll();
        }

        public T Get(int modelId)
        {
            return _modelRepository.GetById(modelId);
        }

        public void Remove(int modelId)
        {
            T model = Get(modelId);

            if(model != null)
            {
                _modelRepository.Delete(model);
                _unitOfWork.Save();
            }
        }

        public void Remove(T model)
        {
            _modelRepository.Delete(model);
            _unitOfWork.Save();
        }

        public T Update(T model)
        {
            var entity = _modelRepository.Update(model);
            _unitOfWork.Save();

            return entity;
        }

        async  public Task<T> AddAsync(T model)
        {
            var entity = await _modelRepository.AddAsync(model);
            await _unitOfWork.SaveAsync();
            return entity;
        }

        async public Task RemoveAsync(T model)
        {
            await _modelRepository.DeleteAsync(model);
            await _unitOfWork.SaveAsync();
        }

        async public Task RemoveAsync(int modelId)
        {
            T model = Get(modelId);

            if (model != null)
            {
                await _modelRepository.DeleteAsync(model);
                await _unitOfWork.SaveAsync();
            }
        }

        public async Task<T> UpdateAsync(T model)
        {
            var entity = await _modelRepository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();

            return entity;
        }
    }
}
