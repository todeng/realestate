﻿using RealEstate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstate.Services.Interfaces
{
    public interface IModelService<T> where T : IBaseEntity
    {
        T Add(T model);
        void Remove(T model);
        void Remove(int modelId);
        T Update(T model);
        IEnumerable<T> GetAll();
        T Get(int modelId);

        Task<T> AddAsync(T model);
        Task RemoveAsync(T model);
        Task RemoveAsync(int modelId);
        Task<T> UpdateAsync(T model);
    }
}
