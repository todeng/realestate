﻿using RealEstate.Domain.Entities;

namespace RealEstate.Services.Interfaces
{
    public interface IApartmentService: IModelService<Apartment>
    {
        void Add(int userId, Apartment model);
    }
}
