﻿using RealEstate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstate.Services.Interfaces
{
    public interface IUserService : IModelService<User>
    {
        User FindByUserName(string userName);
        User FindByProvider(string provider, string providerKey);
    }
}
