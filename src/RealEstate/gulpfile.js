﻿/// <binding AfterBuild='build' Clean='clean' />
"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    path = require('path'),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    less = require('gulp-less'),
    ts = require('gulp-typescript'),
    webroot = "./wwwroot/";

var config = {
    webroot: webroot,
    clientSrc: "./Client/",
    lib: [
        require.resolve('./node_modules/es6-shim/es6-shim.min.js'),
        require.resolve('./node_modules/systemjs/dist/system-polyfills.js'),

        require.resolve('./node_modules/angular2/bundles/angular2-polyfills.js'),
        require.resolve('./node_modules/systemjs/dist/system.src.js'),
        require.resolve('./node_modules/rxjs/bundles/Rx.js'),
        require.resolve('./node_modules/angular2/bundles/angular2.dev.js'),
        require.resolve('./node_modules/angular2/bundles/http.dev.js')
    ]
};

config.js = config.webroot + "js/**/*.js";
config.minJs = config.webroot + "js/**/*.min.js";
config.css = config.webroot + "css/**/*.css";
config.minCss = config.webroot + "css/**/*.min.css";
config.less = "./Client/Styles/**/*.less";
config.lessDest = config.webroot + "css";
config.concatJsDest = config.webroot + "js/site.min.js";
config.concatCssDest = config.webroot + "css/site.min.css";

gulp.task("clean:js", function (cb) {
    rimraf(config.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(config.concatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:css"]);

gulp.task("min:js", function () {
    return gulp.src([config.js, "!" + config.minJs], { base: "." })
        .pipe(concat(config.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    return gulp.src([config.css, "!" + config.minCss])
        .pipe(concat(config.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", ["min:js", "min:css"]);

gulp.task('build:nodelibs', function () {
    return gulp.src(config.lib, { base: config.libBase })
        .pipe(gulp.dest(config.webroot + 'lib/node'));
});

gulp.task('build:less', function () {
    return gulp.src(config.less)
      .pipe(less())
      .pipe(gulp.dest(config.lessDest));
});

gulp.task('build:app:ts', function () {
    var tsProject = ts.createProject('./Client/App/tsconfig.json');
    var tsResult = tsProject.src().pipe(ts(tsProject));

    return tsResult.js.pipe(gulp.dest(config.webroot + "js/app"));
});

gulp.task('build:app:ts', function () {
    var tsProject = ts.createProject('./Client/App/tsconfig.json');
    var tsResult = tsProject.src().pipe(ts(tsProject));

    return tsResult.js.pipe(gulp.dest(config.webroot + "js/app"));
});

gulp.task('build:app:views', function () {
    return gulp.src('./Client/App/views/**/*.html')
        .pipe(gulp.dest(config.webroot + 'js/app/views'));
})

gulp.task("build:app", ["build:app:ts", "build:app:views"])

gulp.task('build', ["build:app", "build:less", "build:nodelibs"]);
