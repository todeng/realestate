﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.DependencyInjection;
using RealEstate.Domain.Entities;
using RealEstate.Identity;
using RealEstate.Identity.ProviderEventHandlers;
using RealEstate.Identity.Providers.Vkontakte;

namespace RealEstate.Configurators
{
    public static class IdentityConfigurator
    {
        public static void AddRealEstateIdentity(this IServiceCollection services)
        {
            services.AddIdentity<User, UserRole>()
                .AddUserStore<AppUserStore>()
                .AddRoleStore<AppRoleStore>();
        }

        public static void UseRealEasteteIdentity(this IApplicationBuilder app)
        {
            app.UseIdentity();

            app.UseFacebookAuthentication(options =>
            {
                options.AppId = "941759285906065";
                options.AppSecret = "382f552fe59542411155418350a4e736";
                options.Scope.Add("public_profile");
                options.UserInformationEndpoint = "https://graph.facebook.com/v2.5/me?fields=id,first_name,last_name,email,gender,name,picture";
                options.SaveTokensAsClaims = true;
                options.Events = new FacebookEventHandler();
            });

            app.UseVkontakteAuthentication(options =>
            {
                options.ClientId = "5302655";
                options.ClientSecret = "jpy3bX0heLsZQFGrmOqO";
                options.Fields.Add("photo_100");
                options.Fields.Add("verified");
                options.Events = new VkontakteEventHandler();
            });
        }
    }
}
