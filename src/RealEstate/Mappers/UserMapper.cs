﻿using RealEstate.Domain.Entities;
using RealEstate.ViewModels.Account;

namespace RealEstate.Mappers
{
    public static class UserMapper
    {
        public static UserViewModel ToVM(this User model)
        {
            return new UserViewModel()
            {
                UserName = model.UserName,
                DisplayName = model.DisplayName,
                PhotoUrl = model.PictureUrl
            };
        }
    }
}
