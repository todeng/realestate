﻿using RealEstate.Domain.Entities;
using RealEstate.ViewModels.RealEstate;

namespace RealEstate.Mappers
{
    public static class RealEstateObjectMapper
    {
        public static ApartmentViewModel ToVm(this Apartment model)
        {
            return new ApartmentViewModel()
            {
                Id = model.Id,
                Caption = model.Caption,
                Longitude = model.Longitude,
                Latitude = model.Latitude
            };
        }

        public static Apartment ToModel(this ApartmentViewModel viewModel)
        {
            return new Apartment()
            {
                Id = viewModel.Id,
                Caption = viewModel.Caption,
                Longitude = viewModel.Longitude,
                Latitude = viewModel.Latitude
            };
        }
    }
}
