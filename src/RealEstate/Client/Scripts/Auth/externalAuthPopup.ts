﻿class ExternalAuthPopup {
    private static options: any = {
        popupWidth: 400,
        popupHeight: 400,
        checkerDelay: 100
    }

    private static popup: Window;

    public static ShowPopup(externalAuthUrl: string, redirectUrl: string) {
        var popup = this.CreatePopup(externalAuthUrl);
        this.InitPopupChecker(popup, redirectUrl);
    }

    private static CreatePopup(url: string): Window {
        if (ExternalAuthPopup.popup == null || ExternalAuthPopup.popup.closed) {
            var x: number = screen.width / 2 - ExternalAuthPopup.options.popupWidth / 2;
            var y = screen.height / 2 - ExternalAuthPopup.options.popupHeight / 2;
            var popupParams = 'height=' + ExternalAuthPopup.options.popupHeight + ',width=' + ExternalAuthPopup.options.popupWidth + ',left=' + x + ',top=' + y;

            ExternalAuthPopup.popup = window.open(url, "", popupParams);
        }
        else {
            ExternalAuthPopup.popup.location.href = url;
        }

        return ExternalAuthPopup.popup;
    }

    private static InitPopupChecker(popup: Window, redirectUrl: string) {
        var tmr = setInterval(function () {
            try {
                if (popup.closed) {
                    clearInterval(tmr);
                    popup = null;
                }

                if (popup.location.pathname + popup.location.search === redirectUrl) {
                    window.location.href = redirectUrl;
                    clearInterval(tmr);
                    popup.close();
                    popup = null;
                }
            }
            catch (err) { }
        }, ExternalAuthPopup.options.checkerDelay)
    }
}