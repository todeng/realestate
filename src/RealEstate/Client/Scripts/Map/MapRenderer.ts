﻿/// <reference path="../DefinitelyTyped/jquery.d.ts" />
/// <reference path="../DefinitelyTyped/leaflet.d.ts" />
/// <reference path="../DefinitelyTyped/leaflet-markercluster.d.ts" />

class MapRendrerer {
    elementId: string;
    map: L.Map;
    markers: any;
    markersList: Array<any>

    constructor(elementId: string) {
        this.elementId = elementId;

        var cloudmadeUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
            cloudmadeAttribution = 'Map data &copy; 2011 OpenStreetMap contributors, Imagery &copy; 2011 CloudMade',
            cloudmade = new L.TileLayer(cloudmadeUrl, { maxZoom: 18 }),
            latlng = new L.LatLng(50.5, 30.51);
        this.map = new L.Map(this.elementId, { center: latlng, zoom: 15, layers: [cloudmade] });

        this.markers = new L.MarkerClusterGroup();
        this.markersList = [];

        this.Render();
    }

    private Render() {
        this.Populate();
        this.map.addLayer(this.markers);
    }

    private Populate() {
        for (var i = 0; i < 100; i++) {
            var m = new L.Marker(this.GetRandomLatLng());
            this.markersList.push(m);
            this.markers.addLayers([m]);
        }
        return false;
    }

    private PopulateRandomVector() {
        for (var i = 0, latlngs = [], len = 20; i < len; i++) {
            latlngs.push(this.GetRandomLatLng());
        }
        var path = new L.Polyline(latlngs);
        this.map.addLayer(path);
    }

    private GetRandomLatLng(): L.LatLng  {
        var bounds = this.map.getBounds(),
            southWest = bounds.getSouthWest(),
            northEast = bounds.getNorthEast(),
            lngSpan = northEast.lng - southWest.lng,
            latSpan = northEast.lat - southWest.lat;

        return new L.LatLng(
            southWest.lat + latSpan * Math.random(),
            southWest.lng + lngSpan * Math.random());
    }

}