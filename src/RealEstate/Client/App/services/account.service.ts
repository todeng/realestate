﻿/// <reference path="../../../node_modules/angular2/typings/browser.d.ts" />
/// <reference path="../../../node_modules/angular2/http.d.ts" />
/// <reference path="../../../node_modules/angular2/core.d.ts" />
/// <reference path="../../../node_modules/rxjs/rx.d.ts" />

import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {User} from '../models/user';
import 'rxjs/Rx';

@Injectable()
export class AccountService {
    private _config = {
        'userInfoUrl': './account/userinfo',
        'logoutUrl': './account/logoff'
    };
    private _currentUser: User;

    get currentUser(): User {
        return this._currentUser;
    }

    constructor(private http: Http) {
        this.initUser();
    }

    getUserInfo() {
        return this.http.post(this._config.userInfoUrl, "")
            .map(res => <User> res.json().data)
            .do(data => console.log(data))
            .catch(this.handleError);
    }

    isAuthenticated() {
        return this.currentUser != null;
    }

    logout() {
        return this.http.post(this._config.logoutUrl, "")
            .toPromise()
            .then(res => {
                this._currentUser = null;
                window.location.href = window.location.href;
            }, this.handleError)
            .catch(this.handleError);
    }

    initUser() {
        return this.getUserInfo()
            .subscribe(
                user => this._currentUser = user);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}