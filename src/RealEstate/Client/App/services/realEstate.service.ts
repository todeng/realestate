﻿import {Injectable} from 'angular2/core';
import {Http, Response, Headers, RequestOptions} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Apartment}     from '../models/apartment';

@Injectable()
export class RealEstateService {
    private _config = {
        apartmentApiUrl: "./api/realestate/apartment"
    }

    constructor(private http: Http) {
    }

    GetAllApartments(id: number) {
        return this.http.get(this._config.apartmentApiUrl)
            .map(res => res.json())
            .do(data => console.log(data))
            .catch(this.handleError);
    }

    GetApartment(id: number) {
        return this.http.get(this._config.apartmentApiUrl + "/" + id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    CreateApartment(apartment: Apartment) {
        let body = JSON.stringify(apartment);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this._config.apartmentApiUrl, body, options)
            .map(res => res.json())
            .do(data => console.log(data))
            .catch(this.handleError);
    }

    UpdateApartment(apartment: Apartment) {
        let body = JSON.stringify(apartment);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.put(this._config.apartmentApiUrl + "/" + apartment.id, body, options)
            .map(res => res.json())
            .do(data => console.log(data))
            .catch(this.handleError);
    }

    private handleError(error) {
        console.log(error)
        return Observable.throw(error.json().error || 'Server error');
    }
}