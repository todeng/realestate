﻿class ExternalAuthPopup {
    private static options: any = {
        popupWidth: 400,
        popupHeight: 400,
        checkerDelay: 100
    }

    private static popup: Window;

    public static ShowPopup(externalAuthUrl: string, redirectUrl: string) {
        this.CreatePopup(externalAuthUrl);
        this.InitPopupChecker(redirectUrl);
    }

    private static CreatePopup(url: string): Window {
        if (ExternalAuthPopup.popup == null || ExternalAuthPopup.popup.closed) {
            var x: number = screen.width / 2 - ExternalAuthPopup.options.popupWidth / 2;
            var y = screen.height / 2 - ExternalAuthPopup.options.popupHeight / 2;
            var popupParams = 'height=' + ExternalAuthPopup.options.popupHeight + ',width=' + ExternalAuthPopup.options.popupWidth + ',left=' + x + ',top=' + y;

            ExternalAuthPopup.popup = window.open(url, "", popupParams);
        }
        else {
            ExternalAuthPopup.popup.location.href = url;
        }

        return ExternalAuthPopup.popup;
    }

    private static InitPopupChecker(redirectUrl: string) {
        var tmr = setInterval(function () {
            try {
                var popup = ExternalAuthPopup.popup;
                if (!popup) {
                    clearInterval(tmr);
                    return;
                }

                if (popup.closed) {
                    clearInterval(tmr);
                    ExternalAuthPopup.DisposePopup();
                }

                if (popup.location.pathname + popup.location.search === redirectUrl) {
                    window.location.href = redirectUrl;
                    clearInterval(tmr);
                    ExternalAuthPopup.DisposePopup();
                }
            }
            catch (err) { }
        }, ExternalAuthPopup.options.checkerDelay)
    }

    private static DisposePopup() {
        if (ExternalAuthPopup.popup) {
            if (ExternalAuthPopup.popup.close) {
                ExternalAuthPopup.popup.close();
            }
            ExternalAuthPopup.popup = null;
        }
    }
}