﻿/// <reference path="../../node_modules/angular2/typings/browser.d.ts" />

import {Component} from 'angular2/core';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {AccountService} from './services/account.service';
import {MapComponent} from './components/map.component';
import {SigninFormComponent} from './components/signinForm.component';
import {ApartmentFormComponent} from './components/apartmentForm.component';


@Component({
    selector: 'app',
    templateUrl: 'js/app/views/app.component.html',
    directives: [MapComponent, SigninFormComponent, ApartmentFormComponent],
    providers: [AccountService, HTTP_PROVIDERS]
})
export class AppComponent {
    _accountService: AccountService;

    constructor(accountService: AccountService) {
        this._accountService = accountService;
    }
}