﻿/// <reference path="../../DefinitelyTyped/leaflet.d.ts" />
/// <reference path="../../DefinitelyTyped/leaflet-markercluster.d.ts" />
/// <reference path="../../../node_modules/angular2/core.d.ts" />

import {Component, ElementRef, QueryList, ViewChildren, SimpleChange, Input, EventEmitter, Output} from 'angular2/core';
import {MapComponent} from './map.component';


@Component({
    selector: 'map-input',
    template: '<map></map>',
    directives: [MapComponent]
})
export class MapInputComponent {
    private _config = {
        defaultLat: 37.9,
        defaultLng: -77
    }

    @ViewChildren(MapComponent) viewChildren: QueryList<MapComponent>;

    isInited: boolean = false;
    map: L.Map;
    marker: L.Marker;

    @Input() latitude: number;
    @Output() latitudeChange = new EventEmitter();
    @Input() longitude: number;
    @Output() longitudeChange = new EventEmitter();

    constructor( ) {
    }

    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        if (this.isInited) {
            if (changes['latitude'] && changes['latitude'].previousValue !== changes['latitude'].currentValue
                || changes['longitude'] && changes['longitude'].previousValue !== changes['longitude'].currentValue) {

                if (!this.latitude || !this.longitude) {
                    return;
                }

                let newPosition = new L.LatLng(this.latitude, this.longitude);
                let currentPositiom = this.marker.getLatLng();

                if (!newPosition.equals(currentPositiom)) {
                    this.marker.setLatLng(newPosition);
                }
            }
        }
    }

    ngAfterViewInit() {
        let _self = this;
        this.map = this.viewChildren.first.map;

        if (!this.latitude || !this.longitude) {

            this.changedPosition(this._config.defaultLat, this._config.defaultLng);
        }

        let latLng = new L.LatLng(this.latitude, this.longitude);

        this.marker = new L.Marker(latLng,{
           draggable: true
        });
        this.marker.addTo(this.map);
        this.map.setView(latLng);


        this.map.on('click', function (e: any) {
            _self.marker.setLatLng(e.latlng);
        });
        this.marker.on('move', function (e: any) {
            let latLng = _self.marker.getLatLng();
            _self.changedPosition(latLng.lat, latLng.lng);
        });

        this.isInited = true;
    }

    private changedPosition(lat: number, lng: number) {
        this.latitude = lat;
        this.longitude = lng;

        this.latitudeChange.emit(lat);
        this.longitudeChange.emit(lng);
    }
}