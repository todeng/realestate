﻿import {Component} from 'angular2/core';

@Component({
    selector: 'sign-in-form',
    templateUrl: 'js/app/views/signinForm.component.html'
})
export class SigninFormComponent {
    email: string;
    password: string;

    constructor() {
    }

    signInByFacebook() {
        console.log(this.email + " facebook");
    }

    signInByGoogle() {
        console.log(this.email + " google");
    }

    signInByVkontakte() {
        console.log(this.email + " vkontakte");
    }
}