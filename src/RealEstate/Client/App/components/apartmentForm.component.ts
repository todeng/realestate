﻿import {Component, ElementRef} from 'angular2/core';
import {RealEstateService} from '../services/realEstate.service';
import {Apartment} from '../models/apartment';
import {MapInputComponent } from './mapInput.component';


@Component({
    selector: 'apartment-form',
    templateUrl: 'js/app/views/apartmentForm.component.html',
    directives: [MapInputComponent],
    providers: [RealEstateService]
})
export class ApartmentFormComponent {

    private model: Apartment;

    constructor(private _realEstateService: RealEstateService) {
        this.model = new Apartment();
    }

    private save() {
        let _self = this;

        if (this.model.id) {
            this._realEstateService.UpdateApartment(this.model)
                .subscribe(
                data => {
                    _self.model = <Apartment>data.data;
                    console.log("Changes have been saved")
                })
            ;
        } else {
            this._realEstateService.CreateApartment(this.model)
                .subscribe(
                data => {
                    _self.model = <Apartment>data.data;
                    console.log("Apartment have been created");
                });
        }
    }

    get diagnostic() { return JSON.stringify(this.model); }
}