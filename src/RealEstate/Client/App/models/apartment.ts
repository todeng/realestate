﻿export class Apartment {
    id: number;
    caption: string;
    longitude: number;
    latitude: number;
}