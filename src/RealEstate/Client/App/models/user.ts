﻿export class User {
    public id: number;
    public username: string;
    public displayName: string;
    public photoUrl: string;

    constructor(id: number, username: string, displayName: string, photoUrl: string) {
        this.id = id;
        this.username = username;
        this.displayName = displayName;
        this.photoUrl = photoUrl;
    }
}