﻿namespace RealEstate.ViewModels.Account
{
    public class UserViewModel
    {
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string PhotoUrl { get; set; }
    }
}
