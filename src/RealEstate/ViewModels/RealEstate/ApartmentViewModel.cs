﻿namespace RealEstate.ViewModels.RealEstate
{
    public class ApartmentViewModel
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }

    }
}
