﻿namespace RealEstate.ViewModels.RealEstate
{
    public class RealEstateBaseObject
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
