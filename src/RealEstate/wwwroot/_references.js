﻿/// <autosync enabled="true" />
/// <reference path="../client/app/app.component.js" />
/// <reference path="../client/app/bootstrap.js" />
/// <reference path="../client/app/components/apartmentform.component.js" />
/// <reference path="../client/app/components/map.component.js" />
/// <reference path="../client/app/components/mapinput.component.js" />
/// <reference path="../client/app/components/signinform.component.js" />
/// <reference path="../client/app/models/apartment.js" />
/// <reference path="../client/app/models/user.js" />
/// <reference path="../client/app/services/account.service.js" />
/// <reference path="../client/app/services/auth/externalauthpopup.js" />
/// <reference path="../client/app/services/realestate.service.js" />
/// <reference path="../client/scripts/app/app.component.js" />
/// <reference path="../client/scripts/app/bootstrap.js" />
/// <reference path="../client/scripts/auth/externalauthpopup.js" />
/// <reference path="../client/scripts/map/maprenderer.js" />
/// <reference path="../gulpfile.js" />
/// <reference path="js/app.js" />
/// <reference path="js/app/app.component.js" />
/// <reference path="js/app/bootstrap.js" />
/// <reference path="js/app/components/apartmentform.component.js" />
/// <reference path="js/app/components/map.component.js" />
/// <reference path="js/app/components/mapinput.component.js" />
/// <reference path="js/app/components/signinform.component.js" />
/// <reference path="js/app/models/apartment.js" />
/// <reference path="js/app/models/user.js" />
/// <reference path="js/app/services/account.service.js" />
/// <reference path="js/app/services/auth/externalAuthPopup.js" />
/// <reference path="js/app/services/map/mapRenderer.js" />
/// <reference path="js/app/services/realestate.service.js" />
/// <reference path="js/auth/externalauthpopup.js" />
/// <reference path="js/map/MapRenderer.js" />
/// <reference path="js/site.js" />
/// <reference path="lib/bootstrap/dist/js/bootstrap.js" />
/// <reference path="lib/jquery/dist/jquery.js" />
/// <reference path="lib/jquery-validation/dist/jquery.validate.js" />
/// <reference path="lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.js" />
/// <reference path="lib/leaflet.awesome-markers/dist/leaflet.awesome-markers.js" />
/// <reference path="lib/leaflet/dist/leaflet-src.js" />
