/// <reference path="../../node_modules/angular2/typings/browser.d.ts" />
System.register(['angular2/core', 'angular2/http', './services/account.service', './components/map.component', './components/signinForm.component', './components/apartmentForm.component'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, account_service_1, map_component_1, signinForm_component_1, apartmentForm_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (account_service_1_1) {
                account_service_1 = account_service_1_1;
            },
            function (map_component_1_1) {
                map_component_1 = map_component_1_1;
            },
            function (signinForm_component_1_1) {
                signinForm_component_1 = signinForm_component_1_1;
            },
            function (apartmentForm_component_1_1) {
                apartmentForm_component_1 = apartmentForm_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(accountService) {
                    this._accountService = accountService;
                }
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'app',
                        templateUrl: 'js/app/views/app.component.html',
                        directives: [map_component_1.MapComponent, signinForm_component_1.SigninFormComponent, apartmentForm_component_1.ApartmentFormComponent],
                        providers: [account_service_1.AccountService, http_1.HTTP_PROVIDERS]
                    }), 
                    __metadata('design:paramtypes', [account_service_1.AccountService])
                ], AppComponent);
                return AppComponent;
            })();
            exports_1("AppComponent", AppComponent);
        }
    }
});
