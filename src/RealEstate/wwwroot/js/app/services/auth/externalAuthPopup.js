var ExternalAuthPopup = (function () {
    function ExternalAuthPopup() {
    }
    ExternalAuthPopup.ShowPopup = function (externalAuthUrl, redirectUrl) {
        this.CreatePopup(externalAuthUrl);
        this.InitPopupChecker(redirectUrl);
    };
    ExternalAuthPopup.CreatePopup = function (url) {
        if (ExternalAuthPopup.popup == null || ExternalAuthPopup.popup.closed) {
            var x = screen.width / 2 - ExternalAuthPopup.options.popupWidth / 2;
            var y = screen.height / 2 - ExternalAuthPopup.options.popupHeight / 2;
            var popupParams = 'height=' + ExternalAuthPopup.options.popupHeight + ',width=' + ExternalAuthPopup.options.popupWidth + ',left=' + x + ',top=' + y;
            ExternalAuthPopup.popup = window.open(url, "", popupParams);
        }
        else {
            ExternalAuthPopup.popup.location.href = url;
        }
        return ExternalAuthPopup.popup;
    };
    ExternalAuthPopup.InitPopupChecker = function (redirectUrl) {
        var tmr = setInterval(function () {
            try {
                var popup = ExternalAuthPopup.popup;
                if (!popup) {
                    clearInterval(tmr);
                    return;
                }
                if (popup.closed) {
                    clearInterval(tmr);
                    ExternalAuthPopup.DisposePopup();
                }
                if (popup.location.pathname + popup.location.search === redirectUrl) {
                    window.location.href = redirectUrl;
                    clearInterval(tmr);
                    ExternalAuthPopup.DisposePopup();
                }
            }
            catch (err) { }
        }, ExternalAuthPopup.options.checkerDelay);
    };
    ExternalAuthPopup.DisposePopup = function () {
        if (ExternalAuthPopup.popup) {
            if (ExternalAuthPopup.popup.close) {
                ExternalAuthPopup.popup.close();
            }
            ExternalAuthPopup.popup = null;
        }
    };
    ExternalAuthPopup.options = {
        popupWidth: 400,
        popupHeight: 400,
        checkerDelay: 100
    };
    return ExternalAuthPopup;
})();
