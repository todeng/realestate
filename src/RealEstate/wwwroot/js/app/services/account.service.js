/// <reference path="../../../node_modules/angular2/typings/browser.d.ts" />
/// <reference path="../../../node_modules/angular2/http.d.ts" />
/// <reference path="../../../node_modules/angular2/core.d.ts" />
/// <reference path="../../../node_modules/rxjs/rx.d.ts" />
System.register(['angular2/core', 'angular2/http', 'rxjs/Observable', 'rxjs/Rx'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, Observable_1;
    var AccountService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            },
            function (_1) {}],
        execute: function() {
            AccountService = (function () {
                function AccountService(http) {
                    this.http = http;
                    this._config = {
                        'userInfoUrl': './account/userinfo',
                        'logoutUrl': './account/logoff'
                    };
                    this.initUser();
                }
                Object.defineProperty(AccountService.prototype, "currentUser", {
                    get: function () {
                        return this._currentUser;
                    },
                    enumerable: true,
                    configurable: true
                });
                AccountService.prototype.getUserInfo = function () {
                    return this.http.post(this._config.userInfoUrl, "")
                        .map(function (res) { return res.json().data; })
                        .do(function (data) { return console.log(data); })
                        .catch(this.handleError);
                };
                AccountService.prototype.isAuthenticated = function () {
                    return this.currentUser != null;
                };
                AccountService.prototype.logout = function () {
                    var _this = this;
                    return this.http.post(this._config.logoutUrl, "")
                        .toPromise()
                        .then(function (res) {
                        _this._currentUser = null;
                        window.location.href = window.location.href;
                    }, this.handleError)
                        .catch(this.handleError);
                };
                AccountService.prototype.initUser = function () {
                    var _this = this;
                    return this.getUserInfo()
                        .subscribe(function (user) { return _this._currentUser = user; });
                };
                AccountService.prototype.handleError = function (error) {
                    console.error(error);
                    return Observable_1.Observable.throw(error.json().error || 'Server error');
                };
                AccountService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], AccountService);
                return AccountService;
            })();
            exports_1("AccountService", AccountService);
        }
    }
});
