System.register(['angular2/core', '../services/realEstate.service', '../models/apartment', './mapInput.component'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, realEstate_service_1, apartment_1, mapInput_component_1;
    var ApartmentFormComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (realEstate_service_1_1) {
                realEstate_service_1 = realEstate_service_1_1;
            },
            function (apartment_1_1) {
                apartment_1 = apartment_1_1;
            },
            function (mapInput_component_1_1) {
                mapInput_component_1 = mapInput_component_1_1;
            }],
        execute: function() {
            ApartmentFormComponent = (function () {
                function ApartmentFormComponent(_realEstateService) {
                    this._realEstateService = _realEstateService;
                    this.model = new apartment_1.Apartment();
                }
                ApartmentFormComponent.prototype.save = function () {
                    var _self = this;
                    if (this.model.id) {
                        this._realEstateService.UpdateApartment(this.model)
                            .subscribe(function (data) {
                            _self.model = data.data;
                            console.log("Changes have been saved");
                        });
                    }
                    else {
                        this._realEstateService.CreateApartment(this.model)
                            .subscribe(function (data) {
                            _self.model = data.data;
                            console.log("Apartment have been created");
                        });
                    }
                };
                Object.defineProperty(ApartmentFormComponent.prototype, "diagnostic", {
                    get: function () { return JSON.stringify(this.model); },
                    enumerable: true,
                    configurable: true
                });
                ApartmentFormComponent = __decorate([
                    core_1.Component({
                        selector: 'apartment-form',
                        templateUrl: 'js/app/views/apartmentForm.component.html',
                        directives: [mapInput_component_1.MapInputComponent],
                        providers: [realEstate_service_1.RealEstateService]
                    }), 
                    __metadata('design:paramtypes', [realEstate_service_1.RealEstateService])
                ], ApartmentFormComponent);
                return ApartmentFormComponent;
            })();
            exports_1("ApartmentFormComponent", ApartmentFormComponent);
        }
    }
});
