/// <reference path="../../DefinitelyTyped/leaflet.d.ts" />
/// <reference path="../../DefinitelyTyped/leaflet-markercluster.d.ts" />
/// <reference path="../../../node_modules/angular2/core.d.ts" />
System.register(['angular2/core', './map.component'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, map_component_1;
    var MapInputComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (map_component_1_1) {
                map_component_1 = map_component_1_1;
            }],
        execute: function() {
            MapInputComponent = (function () {
                function MapInputComponent() {
                    this._config = {
                        defaultLat: 37.9,
                        defaultLng: -77
                    };
                    this.isInited = false;
                    this.latitudeChange = new core_1.EventEmitter();
                    this.longitudeChange = new core_1.EventEmitter();
                }
                MapInputComponent.prototype.ngOnChanges = function (changes) {
                    if (this.isInited) {
                        if (changes['latitude'] && changes['latitude'].previousValue !== changes['latitude'].currentValue
                            || changes['longitude'] && changes['longitude'].previousValue !== changes['longitude'].currentValue) {
                            if (!this.latitude || !this.longitude) {
                                return;
                            }
                            var newPosition = new L.LatLng(this.latitude, this.longitude);
                            var currentPositiom = this.marker.getLatLng();
                            if (!newPosition.equals(currentPositiom)) {
                                this.marker.setLatLng(newPosition);
                            }
                        }
                    }
                };
                MapInputComponent.prototype.ngAfterViewInit = function () {
                    var _self = this;
                    this.map = this.viewChildren.first.map;
                    if (!this.latitude || !this.longitude) {
                        this.changedPosition(this._config.defaultLat, this._config.defaultLng);
                    }
                    var latLng = new L.LatLng(this.latitude, this.longitude);
                    this.marker = new L.Marker(latLng, {
                        draggable: true
                    });
                    this.marker.addTo(this.map);
                    this.map.setView(latLng);
                    this.map.on('click', function (e) {
                        _self.marker.setLatLng(e.latlng);
                    });
                    this.marker.on('move', function (e) {
                        var latLng = _self.marker.getLatLng();
                        _self.changedPosition(latLng.lat, latLng.lng);
                    });
                    this.isInited = true;
                };
                MapInputComponent.prototype.changedPosition = function (lat, lng) {
                    this.latitude = lat;
                    this.longitude = lng;
                    this.latitudeChange.emit(lat);
                    this.longitudeChange.emit(lng);
                };
                __decorate([
                    core_1.ViewChildren(map_component_1.MapComponent), 
                    __metadata('design:type', core_1.QueryList)
                ], MapInputComponent.prototype, "viewChildren", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Number)
                ], MapInputComponent.prototype, "latitude", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], MapInputComponent.prototype, "latitudeChange", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Number)
                ], MapInputComponent.prototype, "longitude", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], MapInputComponent.prototype, "longitudeChange", void 0);
                MapInputComponent = __decorate([
                    core_1.Component({
                        selector: 'map-input',
                        template: '<map></map>',
                        directives: [map_component_1.MapComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], MapInputComponent);
                return MapInputComponent;
            })();
            exports_1("MapInputComponent", MapInputComponent);
        }
    }
});
