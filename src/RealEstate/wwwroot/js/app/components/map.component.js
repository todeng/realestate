/// <reference path="../../DefinitelyTyped/jquery.d.ts" />
/// <reference path="../../DefinitelyTyped/leaflet.d.ts" />
/// <reference path="../../DefinitelyTyped/leaflet-markercluster.d.ts" />
/// <reference path="../../../node_modules/angular2/typings/browser.d.ts" />
System.register(['angular2/core'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var MapComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            MapComponent = (function () {
                function MapComponent(elementRef) {
                    this.InitMap(elementRef.nativeElement);
                }
                MapComponent.prototype.InitMap = function (elementId) {
                    this.elementId = elementId;
                    var cloudmadeUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png', cloudmadeAttribution = 'Map data &copy; 2011 OpenStreetMap contributors, Imagery &copy; 2011 CloudMade', cloudmade = new L.TileLayer(cloudmadeUrl, { maxZoom: 18 }), latlng = new L.LatLng(50.5, 30.51);
                    this.map = new L.Map(this.elementId, { center: latlng, zoom: 15, layers: [cloudmade] });
                    this.markers = new L.MarkerClusterGroup();
                    this.markersList = [];
                    this.Render();
                };
                MapComponent.prototype.Render = function () {
                    this.Populate();
                    this.map.addLayer(this.markers);
                };
                MapComponent.prototype.Populate = function () {
                    for (var i = 0; i < 100; i++) {
                        var m = new L.Marker(this.GetRandomLatLng());
                        this.markersList.push(m);
                        this.markers.addLayers([m]);
                    }
                    return false;
                };
                MapComponent.prototype.PopulateRandomVector = function () {
                    for (var i = 0, latlngs = [], len = 20; i < len; i++) {
                        latlngs.push(this.GetRandomLatLng());
                    }
                    var path = new L.Polyline(latlngs);
                    this.map.addLayer(path);
                };
                MapComponent.prototype.GetRandomLatLng = function () {
                    var bounds = this.map.getBounds(), southWest = bounds.getSouthWest(), northEast = bounds.getNorthEast(), lngSpan = northEast.lng - southWest.lng, latSpan = northEast.lat - southWest.lat;
                    return new L.LatLng(southWest.lat + latSpan * Math.random(), southWest.lng + lngSpan * Math.random());
                };
                MapComponent = __decorate([
                    core_1.Component({
                        selector: 'map',
                        template: '<div></div>'
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef])
                ], MapComponent);
                return MapComponent;
            })();
            exports_1("MapComponent", MapComponent);
        }
    }
});
