System.register([], function(exports_1) {
    var User;
    return {
        setters:[],
        execute: function() {
            User = (function () {
                function User(id, username, displayName, photoUrl) {
                    this.id = id;
                    this.username = username;
                    this.displayName = displayName;
                    this.photoUrl = photoUrl;
                }
                return User;
            })();
            exports_1("User", User);
        }
    }
});
