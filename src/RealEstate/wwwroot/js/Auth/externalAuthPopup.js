var ExternalAuthPopup = (function () {
    function ExternalAuthPopup() {
    }
    ExternalAuthPopup.ShowPopup = function (externalAuthUrl, redirectUrl) {
        var popup = this.CreatePopup(externalAuthUrl);
        this.InitPopupChecker(popup, redirectUrl);
    };
    ExternalAuthPopup.CreatePopup = function (url) {
        if (ExternalAuthPopup.popup == null || ExternalAuthPopup.popup.closed) {
            var x = screen.width / 2 - ExternalAuthPopup.options.popupWidth / 2;
            var y = screen.height / 2 - ExternalAuthPopup.options.popupHeight / 2;
            var popupParams = 'height=' + ExternalAuthPopup.options.popupHeight + ',width=' + ExternalAuthPopup.options.popupWidth + ',left=' + x + ',top=' + y;
            ExternalAuthPopup.popup = window.open(url, "", popupParams);
        }
        else {
            ExternalAuthPopup.popup.location.href = url;
        }
        return ExternalAuthPopup.popup;
    };
    ExternalAuthPopup.InitPopupChecker = function (popup, redirectUrl) {
        var tmr = setInterval(function () {
            try {
                if (popup.closed) {
                    clearInterval(tmr);
                    popup = null;
                }
                if (popup.location.pathname + popup.location.search === redirectUrl) {
                    window.location.href = redirectUrl;
                    clearInterval(tmr);
                    popup.close();
                    popup = null;
                }
            }
            catch (err) { }
        }, ExternalAuthPopup.options.checkerDelay);
    };
    ExternalAuthPopup.options = {
        popupWidth: 400,
        popupHeight: 400,
        checkerDelay: 100
    };
    return ExternalAuthPopup;
})();
