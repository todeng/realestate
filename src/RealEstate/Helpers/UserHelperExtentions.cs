﻿using RealEstate.Identity.Defaults;
using System.Linq;
using System.Security.Claims;

namespace RealEstate.Helpers
{
    public static class UserHelperExtentions
    {
        public static string GetPhotoUrl(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(x => string.Equals(x.Type, AuthProvidersClaims.PhotoUrl))?.Value;
        }

        public static string GetDisplayName(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(x => string.Equals(x.Type, AuthProvidersClaims.DisplayName))?.Value;
        }
    }
}
