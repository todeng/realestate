﻿using Microsoft.AspNet.Mvc;
using RealEstate.Identity.Helpers;
using RealEstate.Mappers;
using RealEstate.Services.Interfaces;
using RealEstate.ViewModels.RealEstate;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstate.Controllers
{
    [Route("api/[controller]")]
    public class RealEstateController : Controller
    {
        private readonly IApartmentService _apartmentService;

        public RealEstateController(IApartmentService apartmentService)
        {
            _apartmentService = apartmentService;
        }

        [HttpGet("apartment")]
        public JsonResult GetAllApartments()
        {
           return new JsonResult(new { success = true,
               data = _apartmentService.GetAll().Select(x => x.ToVm()) });
        }

        [HttpGet("apartment/{id}")]
        public JsonResult GetApartment(int id)
        {
            var apartment = _apartmentService.Get(id);

            return new JsonResult(new
            {
                success = true,
                data = apartment.ToVm()
            });
        }

        [HttpPost("apartment")]
        public async Task<JsonResult> PostApartment([FromBody] ApartmentViewModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                int userId = int.Parse(User.Identities.First().GetNameIdentifer());

                if (ModelState.IsValid)
                {
                    var entity = await _apartmentService.AddAsync(new Domain.Entities.Apartment()
                    {
                        Latitude = model.Latitude,
                        Longitude = model.Longitude,
                        Caption = model.Caption,
                        OwnerId = userId
                    });

                    return new JsonResult(new { success = true, data = entity.ToVm() });
                }
                return new JsonResult(new { success = false, error = ModelState });
            }

            return NotAuthorizedAccess();
        }

        [HttpPut("apartment/{id}")]
        public async Task<JsonResult> PutApartment(int id, [FromBody] ApartmentViewModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                int userId = int.Parse(User.Identities.First().GetNameIdentifer());

                if (ModelState.IsValid)
                {
                    var appartment = _apartmentService.Get(id);

                    if(appartment != null && appartment.OwnerId == userId)
                    {
                        appartment.Caption = model.Caption;
                        appartment.Latitude = model.Latitude;
                        appartment.Longitude = model.Longitude;

                        var entity = await _apartmentService.UpdateAsync(appartment);

                        return new JsonResult(new { success = true, data = entity.ToVm() });
                    }

                    return ObjectNotFound(id);
                }

                return new JsonResult(new { success = false, error = ModelState });
            }

            return NotAuthorizedAccess();
        }

        private JsonResult NotAuthorizedAccess()
        {
            return new JsonResult(new { success = false, error = "Access to this method can have only authorized user" });
        }

        private JsonResult ObjectNotFound(int id)
        {
            return new JsonResult(new { success = false, error = $"Object with id {id} not found" });
        }
    }
}
