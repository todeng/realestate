﻿using RealEstate.Domain.Entities;
using RealEstate.Domain.Repository;
using System.Threading.Tasks;

namespace RealEstate.Domain.UoW
{
    public interface IUnitOfWork
    {
        void Save();
        Task SaveAsync();

        IRepository<User> Users { get; }
        IRepository<T> GetRepository<T>() where T : IBaseEntity;
    }
}
