﻿using Microsoft.Data.Entity;
using System;
using RealEstate.Domain.Entities;
using RealEstate.Domain.Repository;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace RealEstate.Domain.UoW
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private readonly IServiceProvider _serviceProvider;

        public UnitOfWork(IServiceProvider serviceProvider, DbContext dbContext)
        {
            _dbContext = dbContext;
            _serviceProvider = serviceProvider;
        }

        public IRepository<User> Users
        {
            get
            {
                return GetRepository<User>();
            }
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        async public Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public IRepository<T> GetRepository<T>() where T : IBaseEntity
        {
            IRepository<T> repository = _serviceProvider.GetService<IRepository<T>>();

            return repository;
        }
    }
}
