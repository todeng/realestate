﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RealEstate.Domain.Entities
{
    public interface IBaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int Id { get; set; }
    }
}
