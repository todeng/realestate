﻿namespace RealEstate.Domain.Entities
{
    public interface IRealEstateObject
    {
        double Longitude { get; set; }
        double Latitude { get; set; }
        string Caption { get; set; }
        User Owner { get; set; }
    }
}
