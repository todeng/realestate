﻿using System.Collections.Generic;

namespace RealEstate.Domain.Entities
{
    public class User: IBaseEntity
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string PictureUrl { get; set; }
        public string PasswordHash { get; set;}
        public string SecurityStamp { get; set; }
        public string Provider { get; set; }
        public string ProviderKey { get; set; }

        public ICollection<Apartment> Apartments { get; set; }
        public ICollection<House> Houses { get; set; }
        public ICollection<Garage> Garages { get; set; }
        public ICollection<Area> Areas { get; set; }
    }
}
