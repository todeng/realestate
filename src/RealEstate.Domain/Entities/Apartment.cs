﻿namespace RealEstate.Domain.Entities
{
    public class Apartment: IBaseEntity, IRealEstateObject
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Caption { get; set; }

        public int OwnerId { get; set; }
        public User Owner { get; set; }
    }
}
