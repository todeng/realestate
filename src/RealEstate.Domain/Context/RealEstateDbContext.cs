﻿using Microsoft.Data.Entity;
using RealEstate.Domain.Entities;

namespace RealEstate.Domain.Context
{
    public class RealEstateDbContext: DbContext
    {
        public RealEstateDbContext(): base()
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Apartment>()
                .HasOne<User>(x => x.Owner)
                .WithMany(x => x.Apartments)
                .HasForeignKey(x => x.OwnerId);

            builder.Entity<House>()
                .HasOne<User>(x => x.Owner)
                .WithMany(x => x.Houses)
                .HasForeignKey(x => x.OwnerId);

            builder.Entity<Area>()
                .HasOne<User>(x => x.Owner)
                .WithMany(x => x.Areas)
                .HasForeignKey(x => x.OwnerId);

            builder.Entity<Garage>()
                .HasOne<User>(x => x.Owner)
                .WithMany(x => x.Garages)
                .HasForeignKey(x => x.OwnerId);

            base.OnModelCreating(builder);
        }

        public DbSet<User> User { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<Apartment> Apartment { get; set; }
        public DbSet<House> House { get; set; }
        public DbSet<Garage> Garage { get; set; }
        public DbSet<Area> Area { get; set; }
    }
}
