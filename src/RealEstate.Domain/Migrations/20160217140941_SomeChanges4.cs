using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace RealEstate.Domain.Migrations
{
    public partial class SomeChanges4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Provider",
                table: "User",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "Provider", table: "User");
        }
    }
}
