using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace RealEstate.Domain.Migrations
{
    public partial class SomeChanges5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "Provider", table: "User");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Provider",
                table: "User",
                nullable: true);
        }
    }
}
