using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using RealEstate.Domain.Context;

namespace RealEstate.Domain.Migrations
{
    [DbContext(typeof(RealEstateDbContext))]
    partial class RealEstateDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("RealEstate.Domain.Entities.Apartment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<int>("OwnerId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.Area", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<int>("OwnerId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.Garage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<int>("OwnerId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.House", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Name");

                    b.Property<int>("OwnerId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DisplayName");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PictureUrl");

                    b.Property<string>("Provider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("SecurityStamp");

                    b.Property<string>("UserName");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.UserRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.Apartment", b =>
                {
                    b.HasOne("RealEstate.Domain.Entities.User")
                        .WithMany()
                        .HasForeignKey("OwnerId");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.Area", b =>
                {
                    b.HasOne("RealEstate.Domain.Entities.User")
                        .WithMany()
                        .HasForeignKey("OwnerId");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.Garage", b =>
                {
                    b.HasOne("RealEstate.Domain.Entities.User")
                        .WithMany()
                        .HasForeignKey("OwnerId");
                });

            modelBuilder.Entity("RealEstate.Domain.Entities.House", b =>
                {
                    b.HasOne("RealEstate.Domain.Entities.User")
                        .WithMany()
                        .HasForeignKey("OwnerId");
                });
        }
    }
}
