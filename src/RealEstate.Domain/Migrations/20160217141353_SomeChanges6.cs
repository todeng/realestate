using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace RealEstate.Domain.Migrations
{
    public partial class SomeChanges6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Provider",
                table: "User",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "ProviderKey",
                table: "User",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "Provider", table: "User");
            migrationBuilder.DropColumn(name: "ProviderKey", table: "User");
        }
    }
}
