using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace RealEstate.Domain.Migrations
{
    public partial class SomeChanges7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Apartment_User_OwnerId", table: "Apartment");
            migrationBuilder.DropForeignKey(name: "FK_Area_User_OwnerId", table: "Area");
            migrationBuilder.DropForeignKey(name: "FK_Garage_User_OwnerId", table: "Garage");
            migrationBuilder.DropForeignKey(name: "FK_House_User_OwnerId", table: "House");
            migrationBuilder.DropColumn(name: "Name", table: "Garage");
            migrationBuilder.DropColumn(name: "Name", table: "Area");
            migrationBuilder.DropColumn(name: "Name", table: "Apartment");
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "House",
                nullable: false);
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Garage",
                nullable: false);
            migrationBuilder.AddColumn<string>(
                name: "Caption",
                table: "Garage",
                nullable: true);
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Area",
                nullable: false);
            migrationBuilder.AddColumn<string>(
                name: "Caption",
                table: "Area",
                nullable: true);
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Apartment",
                nullable: false);
            migrationBuilder.AddColumn<string>(
                name: "Caption",
                table: "Apartment",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_Apartment_User_OwnerId",
                table: "Apartment",
                column: "OwnerId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Area_User_OwnerId",
                table: "Area",
                column: "OwnerId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Garage_User_OwnerId",
                table: "Garage",
                column: "OwnerId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_House_User_OwnerId",
                table: "House",
                column: "OwnerId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Apartment_User_OwnerId", table: "Apartment");
            migrationBuilder.DropForeignKey(name: "FK_Area_User_OwnerId", table: "Area");
            migrationBuilder.DropForeignKey(name: "FK_Garage_User_OwnerId", table: "Garage");
            migrationBuilder.DropForeignKey(name: "FK_House_User_OwnerId", table: "House");
            migrationBuilder.DropColumn(name: "Caption", table: "Garage");
            migrationBuilder.DropColumn(name: "Caption", table: "Area");
            migrationBuilder.DropColumn(name: "Caption", table: "Apartment");
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "House",
                nullable: true);
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Garage",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Garage",
                nullable: true);
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Area",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Area",
                nullable: true);
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Apartment",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Apartment",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_Apartment_User_OwnerId",
                table: "Apartment",
                column: "OwnerId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Area_User_OwnerId",
                table: "Area",
                column: "OwnerId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Garage_User_OwnerId",
                table: "Garage",
                column: "OwnerId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_House_User_OwnerId",
                table: "House",
                column: "OwnerId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
