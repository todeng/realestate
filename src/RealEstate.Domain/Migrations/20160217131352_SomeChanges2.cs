using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace RealEstate.Domain.Migrations
{
    public partial class SomeChanges2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SecurityStamp",
                table: "User",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "SecurityStamp", table: "User");
        }
    }
}
