using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace RealEstate.Domain.Migrations
{
    public partial class AddedAuthProviders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "User",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "PictureUrl",
                table: "User",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "DisplayName", table: "User");
            migrationBuilder.DropColumn(name: "PictureUrl", table: "User");
        }
    }
}
