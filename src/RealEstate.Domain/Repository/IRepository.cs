﻿using RealEstate.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstate.Domain.Repository
{
    public interface IRepository<T> where T : IBaseEntity
    {
        T GetById(int id);
        IList<T> GetAll();
        IQueryable<T> Get();

        T Add(T obj);

        T Update(T obj);
        void Delete(T obj);
        IEnumerable<T> AddRange(IEnumerable<T> objs);
        void DeleteRange(IEnumerable<T> objs);

        Task<T> AddAsync(T obj);
        Task<T> UpdateAsync(T obj);
        Task DeleteAsync(T obj);
        Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> objs);
        Task DeleteRangeAsync(IEnumerable<T> objs);
    }
}
