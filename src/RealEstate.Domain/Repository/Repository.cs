﻿using Microsoft.Data.Entity;
using RealEstate.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstate.Domain.Repository
{
    public class Repository<T> : IRepository<T> where T : class, IBaseEntity
    {
        private readonly DbContext _context;
        public Repository(DbContext context)
        {
            _context = context;
        }

        public T GetById(int id)
        {
            return _context.Set<T>().SingleOrDefault(e => e.Id == id);
        }

        public IList<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public IQueryable<T> Get()
        {
            return _context.Set<T>();
        }

        public T Add(T obj)
        {
            _context.Set<T>().Add(obj);
            return obj;
        }

        public T Update(T obj)
        {
            var entry = _context.Entry(obj);
            if (entry.State == EntityState.Detached)
            {
                _context.Set<T>().Attach(obj);
            }
            entry.State = EntityState.Modified;
            return obj;
        }

        public void Delete(T obj)
        {
            _context.Set<T>().Remove(obj);
        }

        public IEnumerable<T> AddRange(IEnumerable<T> objs)
        {
            _context.Set<T>().AddRange(objs);
            return objs;
        }

        public void DeleteRange(IEnumerable<T> objs)
        {
            _context.Set<T>().RemoveRange(objs);
        }

        public async Task<T> AddAsync(T obj)
        {
            _context.Set<T>().Add(obj);
            await _context.SaveChangesAsync();
            return obj;
        }

        public async Task<T> UpdateAsync(T obj)
        {
            var entry = _context.Entry(obj);
            if (entry.State == EntityState.Detached)
            {
                _context.Set<T>().Attach(obj);
            }
            entry.State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }

        public async Task DeleteAsync(T obj)
        {
            _context.Set<T>().Remove(obj);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> objs)
        {
            _context.Set<T>().AddRange(objs);
            _context.SaveChanges();
            await _context.SaveChangesAsync();
            return objs;
        }

        public async Task DeleteRangeAsync(IEnumerable<T> objs)
        {
            _context.Set<T>().RemoveRange(objs);
            _context.SaveChanges();
            await _context.SaveChangesAsync();
        }
    }
}
