﻿using Microsoft.Extensions.DependencyInjection;
using RealEstate.Domain.UoW;

namespace RealEstate.IoC.Registries
{
    public static class UowRegister
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}
