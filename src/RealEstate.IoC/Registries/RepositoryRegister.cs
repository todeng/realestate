﻿using Microsoft.Extensions.DependencyInjection;
using RealEstate.Domain.Entities;
using RealEstate.Domain.Repository;

namespace RealEstate.IoC.Registries
{
    public static class RepositoryRegister
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IRepository<User>, Repository<User>>();
            services.AddTransient<IRepository<Apartment>, Repository<Apartment>>();
        }
    }
}
