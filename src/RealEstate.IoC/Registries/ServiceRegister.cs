﻿using Microsoft.Extensions.DependencyInjection;
using RealEstate.Services.Implementations;
using RealEstate.Services.Interfaces;

namespace RealEstate.IoC.Registries
{
    public static class ServiceRegister
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IApartmentService, ApartmentService>();
        }
    }
}
