﻿using Microsoft.Extensions.DependencyInjection;
using RealEstate.IoC.Registries;

namespace RealEstate.IoC
{
    public static class IoCConfigurator
    {
        public static void AddRealEstateServices(this IServiceCollection services)
        {
            RepositoryRegister.Register(services);
            UowRegister.Register(services);
            ServiceRegister.Register(services);
        }
    }
}
